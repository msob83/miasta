<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
/**
 * City
 *
 * @ORM\Table(name="city")
 * @ORM\Entity
 */
class City
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;
    
    /**
     * @var string
     *
     * @ORM\Column(name="parser", type="string", length=45, nullable=true)
     */
    private $parser;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\District", mappedBy="city")
     */
    private $districts;
    
    
    function setName($name) {
        $this->name = $name;
    }

    
    function getName() {
        return $this->name;
    }

    function getId() {
        return $this->id;
    }
    
    function getUrl() {
        return $this->url;
    }

    function setUrl($url) {
        $this->url = $url;
    }

    function getParser() {
        return $this->parser;
    }

    function setParser($parser) {
        $this->parser = $parser;
    }

            
    public function  __toString(){
        return $this->name;
    }
    
    
    /**
     * @return Collection|District[]
     */
    public function getDistricts()
    {
        return $this->districts;
    }
    
     public function __construct()
    {
        $this->districts = new ArrayCollection();
    }
    
}

