<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * District
 *
 * @ORM\Table(name="district", indexes={@ORM\Index(name="fk_city_idx", columns={"city_id"})})
 * @ORM\Entity
 */
class District
{
    /**
     * @var integer
     *
     * @ORM\Column(name="population", type="integer", nullable=true)
     */
    private $population;

    /**
     * @var float
     *
     * @ORM\Column(name="area", type="float", precision=10, scale=0, nullable=true)
     */
    private $area;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\City
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\City",inversedBy="districts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $city;

    public function getPopulation() {
        return $this->population;
    }

    public function getArea() {
        return $this->area;
    }

    public function getName() {
        return $this->name;
    }

    public function getCity() {
        
        return $this->city;
    }

    public function setPopulation($population) {
        $this->population = $population;
    }

    public function setArea($area) {
        $this->area = $area;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setCity(\AppBundle\Entity\City $city) {
        $this->city = $city;
    }

    public function getId() {
        return $this->id;
    }
    
    public function __toString() {
        return $this->name;
    }


}

