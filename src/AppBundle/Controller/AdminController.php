<?php

namespace AppBundle\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends BaseAdminController
{
    protected function searchAction() {

        $this->_disable_filters($this->request->query->get('filter_conditions'));
        
        return parent::searchAction();
        
    }
    
    protected function _disable_filters($conditions){
        if(empty($conditions)){
            return;
        }
       
        foreach($this->entity['search']['fields'] as $name_full=>$field){
            $name_ar = explode('.',$name_full);
            $name = $name_ar[0];
            
            if(!in_array($name,$conditions)){
                unset($this->entity['search']['fields'][$name_full]);
            }
        }
        
    }
}
