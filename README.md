Instrukcja.
Aplikacja została napisane we frameworku Symfony 3.4
Demo aplikacji można testować pod adresem  http://miasta.msob.usermd.net/

Serwis korzysa z bazy danych mysql.
W bazie danych są dwie tabele city oraz district. 
Załączono zrzuty tych tabel w repo razem z  przykładowymi wartościami. 

W tabeli district przechowywanę są pobrane informacje takie jak
populacja, powierzchnia, nazwa oraz klucz obcy do odpowiadającego dzielnicy miasta.

W tabeli city przehowywana jest nie tylko nazwa miasta ale też url początkowy do parsowania
oraz nazwa klasy parsera. Zrobiłem tak, że jeśli parser nazywa się xyzParser to w bazie danych jest xyz.
Jeśli w przyszłości chcesz parsować kolejne miasto to dodajesz miasto za pomocą CRUD, wprowadzasz nazwę klasy parsera, który stworzysz.
Parsery są w App/Bundle/Parsers. Umieściłem tam też inne pomocnicze klasy i interfejsy.
Każdy nowy parser powinien dziedziczyć po bazowym parserze, nadpisując tylko kilka zmieniających się metod, gdzie kod
zależy od indywidualnie od zaczytywanej strony html.

Problemy były w Krakowie, gdzie był iframe i jsp ale udało się uff ;)

Parsery wykorzystują Symfony\Component\DomCrawler\Crawler oraz dodatkowo wykorzystane zwykłe wyrażnia regularne czyli preg_match_all.
Do samego pobierania htmla  wykorzystano curla.

--
Domyślnie ustawiłem w routingu, że na dzień dobry mamy panel admina.
Wykorzystałem tutaj   easycorp/easyadmin-bundle, ponieważ jest idealny do szybkich CRUDów w SF.
Na potrzeby tego zadanka Sonata by była za ciężka.
W panelu można sortować po dowolnej kolumnie oraz wyszukiwać po dowolnej kolumnie - 
zaznaczonej w checkboxach - trzeba było trochę rozszerzyć EasyAdminBundle o ten ficzer, stąd kontroler AppBundleAdminController oraz nadpisujące widoki w Resources.

Przykładowo we frameworku YII2 lista ma sortowanie i filtrowanie po dowolnej kolumnie w standardzie, ale wybrałem SF34, żeby było ciekawiej a pozatym wiadomo dlaczego ;)


Powierzchnia jest w jednostkach km2. W krakowie były hektary ale zostały przeliczone podczas parsowania i zapisane do bazy w km2.

Aplikacja pozwala parsować dzielnice Gdańska i Krakowa na dwa sposoby.
Albo klikamy w czerwony guzik z napisem "CLICK ME TO..." w panelu albo korzystamy z konsoli.
Aby uruchomić parser z konsoli wystarczy wprowadzić komendę
php bin/console app:crawler  

Klasa odpowiedzialna za konsolę jest to AppBundle/Command/CrawlerCommand. 
Entity Manager został tam wstrzyknięty jako serwis, ponieważ inaczej nie działałoby jego wywołanie w kontrolerze z poziomu konsoli.

Parsowanie tworzy nowe wpisy jesli danej dzielnicy nie ma i robi update tam, gdzie dana dzielnica jest.
Sprawdzenie czy dzielnica jest już czy jej nie ma w bazie danych to porównywanie nazw.



Miłego dnia.
Pozdrawiam
Michał Sobański


--------------------------------------------------------------------------------
Symfony Standard Edition
========================

**WARNING**: This distribution does not support Symfony 4. See the
[Installing & Setting up the Symfony Framework][15] page to find a replacement
that fits you best.

Welcome to the Symfony Standard Edition - a fully-functional Symfony
application that you can use as the skeleton for your new applications.

For details on how to download and get started with Symfony, see the
[Installation][1] chapter of the Symfony Documentation.

What's inside?
--------------

The Symfony Standard Edition is configured with the following defaults:

  * An AppBundle you can use to start coding;

  * Twig as the only configured template engine;

  * Doctrine ORM/DBAL;

  * Swiftmailer;

  * Annotations enabled for everything.

It comes pre-configured with the following bundles:

  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**TwigBundle**][8] - Adds support for the Twig templating engine

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][13] (in dev env) - Adds code generation
    capabilities

  * [**WebServerBundle**][14] (in dev env) - Adds commands for running applications
    using the PHP built-in web server

  * **DebugBundle** (in dev/test env) - Adds Debug and VarDumper component
    integration

All libraries and bundles included in the Symfony Standard Edition are
released under the MIT or BSD license.

Enjoy!

[1]:  https://symfony.com/doc/3.4/setup.html
[6]:  https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
[7]:  https://symfony.com/doc/3.4/doctrine.html
[8]:  https://symfony.com/doc/3.4/templating.html
[9]:  https://symfony.com/doc/3.4/security.html
[10]: https://symfony.com/doc/3.4/email.html
[11]: https://symfony.com/doc/3.4/logging.html
[13]: https://symfony.com/doc/current/bundles/SensioGeneratorBundle/index.html
[14]: https://symfony.com/doc/current/setup/built_in_web_server.html
[15]: https://symfony.com/doc/current/setup.html
