<?php

namespace  AppBundle\Parsers;

use AppBundle\Parsers\ParserableInterface;
use AppBundle\Parsers\CurlableInterface;

class CityParserBase implements ParserableInterface{
    /**
     * @var \AppBundle\Entity\City
     */
    protected $city;
    
    /**
     * @var \AppBundle\Parsers\CurlableInterface
     */
    protected $curl;
    
    /**  
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    
    
    public function __construct(\AppBundle\Entity\City $city, \Doctrine\ORM\EntityManager $em, CurlableInterface $curl) {
        $this->city = $city;
        $this->curl = $curl;
        $this->em = $em;
       
    }
    
    public function parse(){
       $url = $this->city->getUrl();
       if(empty($url)){
           return false;
       }
       $html = $this->curl->getHtmlFromUrl($url);
       
       $this->_customParse($html);
       
      
    }
    
    public function getCityName() {
        return $this->city->getName();
    }
    
    protected function _customParse($html){
        
        return false;
    }
    
    
    protected function _saveOrUpdateDB($data){
        if(empty($data['districtName']) || empty($data['districtPopulation']) || empty($data['districtArea']) ){
            return false;
        }
        
        $data['districtName'] = strip_tags(html_entity_decode($data['districtName']));
        $data['districtPopulation'] = strip_tags($data['districtPopulation']);
        $data['districtArea'] = strip_tags($data['districtArea']);
        
       
        $district = $this->em->getRepository('AppBundle:District')->findOneBy(['city'=>$this->city,'name'=>$data['districtName']]);
        if(empty($district)){
            //nowy wpis
            $district =  new \AppBundle\Entity\District();
            $district->setCity($this->city);
           
        }
           
        $district->setName($data['districtName']);
        $district->setArea($data['districtArea']);
        $district->setPopulation($data['districtPopulation']);
                 
        $this->em->persist($district);
        $this->em->flush();
        
    }
}

