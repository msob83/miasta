<?php

namespace AppBundle\Parsers;
use  AppBundle\Parsers\CityParserBase;
use  Symfony\Component\DomCrawler\Crawler;

class CracowParser extends CityParserBase{
    
    protected function _customParse($html) {
        
        $crawler = new Crawler($html);

 
        /**
         * tutaj mamy iframe gdzie parsujemy adres z src
         */
        
        $iframe = $crawler->filter('iframe[name="FRAME4"]')->eq(0);
        if(!empty($iframe->attr("src"))){
             $this->_customParse2('http:'.$iframe->attr("src"));
        }
    }
    
    protected function _customParse2($url){
        $html = $this->curl->getHtmlFromUrl($url);
        //echo $html;
        $base = 'http://'.parse_url($url)['host'].parse_url($url)['path'];
        $base_ar = explode('/',$base);
        array_pop($base_ar);
       
        $base = implode('/',$base_ar).'/';
        $crawler =  new Crawler($html);
        $form = $crawler->filter('#mainDiv form')->eq(0);
        $action = $form->attr('action');
        if(empty($action)){
           return; 
        }
        foreach($form->filter('select option')->getIterator() as $option){
            if(!empty($option->getAttribute("value"))){
                $this->_customParseDetail($base.$action.'?id='.$option->getAttribute("value"));
            }
        }
    }
    
    protected function _customParseDetail($url){
       $html = $this->curl->getHtmlFromUrl($url);
       $crawler = new Crawler($html);
       
        $districtName = '';
        $districtArea = 0;
        $districtPopulation = 0;
        $matches = [];
        foreach(  $crawler->filter('.tabela_ogloszenia_srodek tr td ')->getIterator() as $i=>$td){
            if($i == 2){
                $districtArea = trim(str_replace(',','.',str_replace('ha','',$td->textContent)));
                // z hektarów na km2
                $districtArea = $districtArea*0.01;
            }elseif($i == 4){
                $districtPopulation = trim($td->textContent);         
            }
        
        }
       
        preg_match_all('/<h3>(.*)<\/h3>/Uis', $html,$matches);
        if(!empty($matches[1][0])){
             $districtName = trim($matches[1][0]);
             $districtNameAr = explode('&nbsp;',$districtName);
             if(isset($districtNameAr[1])){
                 $districtName = $districtNameAr[1];
             }else{
                  $districtName = $districtNameAr[0];
             }
         }
         $this->_saveOrUpdateDB([
            'districtName' => $districtName,
            'districtArea' => $districtArea,
            'districtPopulation'=>$districtPopulation
        ]);
  
    }
    
}
