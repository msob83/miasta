-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: mysql12.mydevil.net    Database: m1056_miasta
-- ------------------------------------------------------
-- Server version	5.7.16-10-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `district`
--

DROP TABLE IF EXISTS `district`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `population` int(11) DEFAULT NULL,
  `area` float DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_idx` (`city_id`),
  CONSTRAINT `fk_city` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `district`
--

LOCK TABLES `district` WRITE;
/*!40000 ALTER TABLE `district` DISABLE KEYS */;
INSERT INTO `district` VALUES (1,33359,5.5676,'Stare Miasto',2),(2,28960,5.8452,'Grzegórzki',2),(3,47100,6.4379,'Prądnik Czerwony',2),(4,69661,23.4187,'Prądnik Biały',2),(5,30885,5.619,'Krowodrza',2),(6,23594,9.5596,'Bronowice',2),(7,20336,28.731,'Zwierzyniec',2),(8,60495,46.1887,'Dębniki',2),(9,15256,5.4151,'Łagiewniki-Borek Fałęcki',2),(10,26538,25.604,'Swoszowice',2),(11,53339,9.54,'Podgórze Duchackie',2),(12,63166,18.4739,'Bieżanów-Prokocim',2),(13,35045,25.6671,'Podgórze',2),(14,27369,12.2568,'Czyżyny',2),(15,52426,5.59,'Mistrzejowice',2),(16,42106,3.699,'Bieńczyce',2),(17,20192,23.8155,'Wzgórza Krzesławickie',2),(18,53120,65.4099,'Nowa Huta',2),(19,4608,2.31,'Aniołki',1),(20,7469,7.08,'Brętowo',1),(21,12287,2.74,'Brzeźno',1),(22,51055,11.08,'Chełm',1),(23,16269,11.48,'Jasień',1),(24,9302,19.84,'Kokoszki',1),(25,1877,8.38,'Krakowiec-Górki Zachodnie',1),(26,1280,4.03,'Letnica',1),(27,5831,14.43,'Matarnia',1),(28,2846,4.18,'Młyniska',1),(29,9695,2.28,'Nowy Port',1),(30,15613,18.4,'Oliwa',1),(31,3041,7.97,'Olszynka',1),(32,14435,19.63,'Orunia-Św. Wojciech-Lipce',1),(33,15393,14.13,'Osowa',1),(34,26403,4.32,'Piecki-Migowo',1),(35,4134,7.1,'Przeróbka',1),(36,15005,2.33,'Przymorze Małe',1),(37,27693,3.12,'Przymorze Wielkie',1),(38,1186,14.19,'Rudniki',1),(39,13052,2.64,'Siedlce',1),(40,10725,10.96,'Stogi',1),(41,5361,1.09,'Strzyża',1),(42,10505,1.4,'Suchanino',1),(43,25950,5.62,'Śródmieście',1),(44,24794,7.79,'Ujeścisko-Łostowice',1),(45,3658,3.01,'VII Dwór',1),(46,22924,3.52,'Wrzeszcz Dolny',1),(47,22195,6.46,'Wrzeszcz Górny',1),(48,3346,35.79,'Wyspa Sobieszewska',1),(49,2487,0.53,'Wzgórze Mickiewicza',1),(50,13265,1.23,'Zaspa-Młyniec',1),(51,12951,2.03,'Zaspa-Rozstaje',1),(52,16643,2.33,'Żabianka-Wejhera-Jelitkowo-Tysiąclecia',1);
/*!40000 ALTER TABLE `district` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-07 19:42:39
