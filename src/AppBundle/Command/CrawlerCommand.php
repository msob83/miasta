<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Doctrine\ORM\EntityManager;

class CrawlerCommand extends Command
{
    /**
     *
     * @var EntityManager 
     */
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
       
        $this->em = $em;
        // best practices recommend to call the parent constructor first and
        // then set your own properties. That wouldn't work in this case
        // because configure() needs the properties set in this constructor
        $this->requirePassword = false;

        parent::__construct();
    }
    
    protected function configure()
    {
        
         $this
        // the name of the command (the part after "bin/console")
        ->setName('app:crawler')

        // the short description shown while running "php bin/console list"
        ->setDescription('Starts crawling distrincts')

        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('This command allows you to get all the districts in Cracow and Gdansk ')
    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('No siema! Zaycznam parsować Gdańsk i Kraków');
        $controller = new \AppBundle\Controller\DefaultController();
        $controller->consoleAction($this->em);
        $output->writeln('Koniec ;)!');
        
        
    }
}