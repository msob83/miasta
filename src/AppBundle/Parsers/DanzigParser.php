<?php

namespace AppBundle\Parsers;
use  AppBundle\Parsers\CityParserBase;
use  Symfony\Component\DomCrawler\Crawler;


class DanzigParser extends CityParserBase{
    
    protected function _customParse($html) {
        
        $crawler = new Crawler($html);

        
        foreach($crawler->filter(".lista-dzielnic li")->getIterator() as $i=>$li){
            if($i > 0){// pierwszy element to wszystkie - i nie działa
                //w li są linki a 
                if($li->getElementsByTagName('a')->item(0) !==null &&  $li->getElementsByTagName('a')->item(0)->hasAttributes()){
                    $this->_customParseDetail('http://'.parse_url($this->city->getUrl())['host'].'/'.$li->getElementsByTagName('a')->item(0)->attributes->getNamedItem('href')->nodeValue);                   
                }
                /*
                 * matarnia jest spanem bo jest wybrana na starcie
                 * lepiej by było zaczać od gdansk.pl/dzielnice ale ok
                 */
                if($li->getElementsByTagName('span')->item(0) !==null &&  $li->getElementsByTagName('span')->item(0)->hasAttributes()){
                    $this->_customParseDetail('http://'.parse_url($this->city->getUrl())['host'].'/'.$li->getElementsByTagName('span')->item(0)->attributes->getNamedItem('href')->nodeValue);                   
                }
            }          
        } 

    }
    
    protected function _customParseDetail($url){
        
      
        $html  = $this->curl->getHtmlFromUrl($url);
        $crawler = new Crawler($html);
        
        $districtName = '';
        $districtArea = 0;
        $districtPopulation = 0;
        $matches = [];
        
        foreach($crawler->filter('div.opis div')->getIterator() as $i=>$div){
            echo $i;
            $text = $div->textContent;
            if($i == 0){
                $districtName = trim($text);
            }else{
                preg_match_all('/Powierzchnia:\s*(\d+\.\d{1,2})\s*km2/Uis', str_replace(',','.',$text),$matches);
                if(!empty($matches[1][0])){
                    $districtArea = trim($matches[1][0]);
                }
                $matches = [];
                preg_match_all('/ludności:\s*(\d+)\s*osób/Uis', str_replace(',','.',$text),$matches);
                if(!empty($matches[1][0])){
                    $districtPopulation = trim($matches[1][0]);
                }
            }
//            echo $div->textContent.'<br />';
        }
//        echo 'name '.$districtName.'<br />';
//        echo 'area '.$districtArea.'<br />';
//        echo 'population '.$districtPopulation.'<br />';
        
        $this->_saveOrUpdateDB([
            'districtName' => $districtName,
            'districtArea' => $districtArea,
            'districtPopulation'=>$districtPopulation
        ]);
  
        
    }
    
    

    
}