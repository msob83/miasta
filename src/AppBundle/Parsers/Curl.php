<?php

namespace AppBundle\Parsers;

use AppBundle\Parsers\CurlableInterface;

class Curl implements CurlableInterface {

    public function getHtmlFromUrl($url) {
        return $this->_goCurlGet($url);
    }

    protected function _goCurlGet($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);

        return $server_output;
    }

}
