<?php
namespace  AppBundle\Parsers;

interface CurlableInterface{
    public function getHtmlFromUrl($url);
}
