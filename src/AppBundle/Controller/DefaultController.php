<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
    /**
     * @Route("/start", name="start")
     */
    public function startAction(Request $request)
    {
       
        $this->_parse();
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);

    }
    
    
     /**
     * @Route("/console", name="console")
     */
    public function consoleAction($em = null)
    {
        
        $this->_parse($em);
       // echo PHP_EOL.'Done';
        //die();

    }
    
    protected function _parse(\Doctrine\ORM\EntityManagerInterface $em = null){ 
        if(empty($em)){
            $em = $this->getDoctrine()->getManager();
        }
        
        
        $cities = $em->getRepository('AppBundle:City')->findAll();
        $curl = new \AppBundle\Parsers\Curl();
        
        foreach($cities as $city){
            
              
                if($parser = \AppBundle\Parsers\CityParserFactory::getParser($city,$em,$curl)){  
                    $parser->parse();
                    
                    echo $parser->getCityName().PHP_EOL;
                }else{
                    echo 'Brak parsera dla miasta '.$city->getName();
                }
        }
        
    }
}
