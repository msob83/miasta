<?php
namespace  AppBundle\Parsers;

interface ParserableInterface{
    public function parse();
}
