<?php

namespace AppBundle\Parsers;

class CityParserFactory {

    public static function getParser($city, \Doctrine\ORM\EntityManager $em, \AppBundle\Parsers\Curl $curl) {
        if (empty($city->getParser())) {
            return false;
        }

        $className = 'AppBundle\\Parsers\\' . $city->getParser() . 'Parser';
        if (!class_exists($className)) {
            return false;
        }
        return new $className($city, $em, $curl);
    }

}
